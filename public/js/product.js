$( document ).ready(function() {

  if($('#productForm').length > 0) {
    $('#productForm').validate({
      rules:{
        name : {
          required : true,
          minlength: 3,
          alphanumeric:true,
          maxlength: 32
        },
        sku : {
          required : true,
          alphanumeric: true
        },
        price :{
          required : true,
          number: true
        },
        install_fee: {
          required: true,
          number: true
        },
        quantity: {
          required: true,
          number: true
        },
        description :{
          required: true
        },
        image:{
          accept:"image/*"
        }
      }
    });
  }

    $('#saveProduct').click(function() {
      if($('#productForm').valid()){
        $('#productForm').submit();
      }
    });

  });

//confirm delete
function confirmDelete(product)
{
  if(!confirm("Are you sure you want to delete this product ?"))
  {
    return false;
  } else {
    deleteProduct(product);
  }
}

// delete product
function deleteProduct(product) {
  let url = $(product).attr('href');

  $.ajax({
    method: 'delete',
    url: url,
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
      if(data.success === true)
      {
        location.reload();
      }
    },
    error: function(err){
      alert('Something went wrong. Please try again.');
    }
  });
}
