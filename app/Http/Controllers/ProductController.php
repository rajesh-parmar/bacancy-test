<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Redirect;
use Session;
use Storage;
use URL;
use DataTables;
use Excel;
use Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.products');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'name' => 'required|alpha_num|max:255',
            'sku' => 'required|alpha_num',
            'price' => 'required|regex:/(^\d{1,10}(\.\d{1,2})?$)/',
            'install_fee' => 'required|numeric',
            'quantity' => 'required|numeric',
            'description' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png',
        ]);

        if ($request->hasFile('image')) {

            $originalName = $request->file('image')->getClientOriginalName();

            $imageName = time(). '-' . $originalName;

            $file = $request->file('image');

            Storage::put(
                'images/products/'.$imageName,
                file_get_contents($file->getRealPath())
            );

            $image_path = 'products/'.$imageName;
        }

        $product = new Product();

        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->price = $request->price;
        $product->install_fee = $request->install_fee;
        $product->quantity = $request->quantity;
        $product->description = $request->description;
        $product->image = $image_path;

        $product->save();

        $request->session()->flash('message', 'Product added successfully!');

        return Redirect::to('products');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.product')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product)
    {
        return view('products.edit')->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //validation
        $request->validate([
            'name' => 'required|alpha_num|max:255',
            'sku' => 'required|alpha_num',
            'price' => 'required|regex:/(^\d{1,10}(\.\d{1,2})?$)/',
            'install_fee' => 'required|numeric',
            'quantity' => 'required|numeric',
            'description' => 'required',
            'image' => 'mimes:jpeg,jpg,png',
        ]);

        $product->name = $request->name;
        $product->sku  = $request->sku;
        $product->price  = $request->price;
        $product->install_fee  = $request->install_fee;
        $product->quantity  = $request->quantity;
        $product->description  = $request->description;

        if ($request->hasFile('image')) {

            $originalName = $request->file('image')->getClientOriginalName();

            $imageName = time(). '-' . $originalName;

            $file = $request->file('image');

            Storage::put(
                'images/products/'.$imageName,
                file_get_contents($file->getRealPath())
            );

            $image_path = 'products/'.$imageName;

            Storage::delete('images/'. $product->image);

            $product->image = $image_path;
        }

        $product->save();

        $request->session()->flash('message', 'Product updated successfully!');

        return Redirect::to('products');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        $product->delete();

        $request->session()->flash('message', 'Product deleted successfully!');

        return Response::json(['success' => true]);
    }

    /**
     * Generate jquery datatable
     *
     */
    public function getProducts() {
        $products = Product::select(['id', 'name', 'sku', 'price', 'install_fee', 'quantity' ,'created_at', 'updated_at']);

        return Datatables::of($products)
            ->addColumn('action', function ($product) {
                $btns= '<div class="action_btns">';
                $btns .= '<a href="/products/'.$product->id.'" class="btn btn-sm btn-info">View</a>';
                $btns .= '<a href="/products/'.$product->id.'/edit" class="btn btn-sm btn-warning">Edit</a>';

                $btns .= '<button href="/products/'.$product->id.'" onclick="confirmDelete(this)" class="btn btn-sm btn-danger" product_id='. $product->id .'">Delete</button>';
                $btns .= '</div>';
                return $btns;
            })
            ->make(true);
    }

    /**
     * Import excel or csv file
     *
     */
    public function importCsvExcel(Request $request) {
        try {
            \Log::info('Importing file...');
            if ($request->hasFile('excelFile')) {
                $no = 0;
                Excel::load($request->file('excelFile')->getRealPath(), function ($reader) use($no, $request) {
                    foreach ($reader->toArray() as $key => $row) {
                        \Log::debug('data', [
                            'data' => $row
                        ]);
                        if (! empty($row['name']) && !empty($row['sku'])) {

                            $product = new Product();
                            $product->sku = $row['sku'];
                            $product->name = $row['name'];
                            $product->price = !empty($row['price']) ? $row['price'] : NULL;
                            $product->install_fee = !empty($row['install_fee']) ? $row['install_fee'] : NULL;
                            $product->quantity = !empty($row['quantity']) ? $row['quantity'] : NULL;
                            $product->description = !empty($row['description']) ? $row['description'] : NULL;
                            $product->save();
                            $no++;

                            \Log::debug('Product added.', [
                                'product_id' => $product->id,
                            ]);
                        }
                    }

                    if($no > 0) {
                        $request->session()->flash('message', 'File imported successfully');
                    } else {
                        $request->session()->flash('message', 'Sorry, File not imported! Pleaser provide valid data.');
                    }
                });

                return Redirect::to('products');
            } else {
                $request->session()->flash('message', 'File not found!');
                return Redirect::to('products');
            }
        } catch (\Exception $e) {
            $request->session()->flash('message', $e->getMessage());
            return Redirect::to('products');
        }
    }
}
