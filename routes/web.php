<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('getproducts', 'ProductController@getProducts');
    Route::post('import-file', 'ProductController@importCsvExcel')->name('import-file');
    Route::resource('products', 'ProductController');
    Route::get('/home', 'HomeController@index')->name('home');

});