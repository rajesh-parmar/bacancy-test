@extends('layouts.app')
@section('content')
  <div class="container">
    <section>
      <h3 class="text-center">Edit product</h3>
    </section>
    <div class="row">
      <div class="col-lg-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form name="productForm" id="productForm" method="post" enctype="multipart/form-data" action="{{ route('products.update', $product->id) }}" autocomplete="off">
          {{ method_field('PUT') }}
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter product name" name="name" value="{{ old('name') ? old('name') : $product->name }}">
          </div>

          <div class="form-group">
            <label for="sku">Sku:</label>
            <input type="text" class="form-control" id="sku" placeholder="Enter product sku" name="sku" value="{{ old('sku') ? old('sku') : $product->sku }}">
          </div>

          <div class="form-group">
            <label for="price">Price:</label>
            <input type="number" class="form-control" id="price" placeholder="Enter product price" name="price" value="{{ old('price') ? old('price') : $product->price }}">
          </div>

          <div class="form-group">
            <label for="install_fee">Install fee:</label>
            <input type="number" class="form-control" id="install_fee" placeholder="Enter install fee" name="install_fee" value="{{ old('install_fee') ? old('install_fee') : $product->install_fee }}">
          </div>

          <div class="form-group">
            <label for="quantity">Quantity:</label>
            <input type="number" class="form-control" id="quantity" placeholder="Enter enter quantity" name="quantity" value="{{ old('quantity') ? old('quantity') : $product->quantity }}">
          </div>

          <div class="form-group">
            <label for="description">Description:</label>
            <textarea name="description" id="description" class="form-control">{{ old('description') ? old('description') : $product->description }}</textarea>
          </div>

          <div class="form-group">
            <label for="image">Image:</label>
            <input type="file" class="form-control" id="image" name="image" accept="image/*">
            <img src="/storage/images/{{ $product->image}}" class="rounded" alt="Cinque Terre" width="200" height="200">
          </div>

          <div class="buttons text-center">
            <button type="button" name="saveProduct" id="saveProduct" class="btn btn-success">Save</button>
            <a href="{!! URL::previous() !!}" class="btn btn-secondary">
                Cancel
            </a>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('plugins/validation/validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/validation/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/product.js') }}"></script>
@endpush
