@extends('layouts.app')
@section('content')
  <div class="container">
    <section>
      <h3 class="text-center">{{ $product->name }}</h3>
    </section>
    <br>
    <div class="row">
      <div class="col-lg-12">
        <div class="text-center">
          <img src="/storage/images/{{ $product->image}}" class="rounded" alt="Cinque Terre" width="200" height="200" accept="image/*">
        </div>
        <br>
        <p>
          <strong>SKU: </strong> {{$product->sku}}
        </p>
        <p>
          <strong>Price: </strong> {{$product->price}}
        </p>
        <p>
          <strong>Installation fee: </strong> {{$product->installation_fee}}
        </p>
        <p>
          <strong>Qty: </strong> {{$product->quantity}}
        </p>
        <p>
          <strong>Description: </strong> {{$product->description}}
        </p>
        <p>
          <strong>Created date: </strong> {{$product->created_at}}
        </p>
        <p>
          <strong>Updated date: </strong> {{$product->updated_at}}
        </p>

        <br>
        <a href="{!! URL::previous() !!}" class="btn btn-secondary">
                Back
            </a>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('plugins/validation/validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/validation/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/product.js') }}"></script>
@endpush
