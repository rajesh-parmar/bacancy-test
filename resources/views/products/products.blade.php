@extends('layouts.app')
@section('content')
  @push('additional-css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/Buttons-1.5.4/css/buttons.bootstrap4.min.css') }}">
  @endpush
  <div class="container">
    <section style="margin-bottom: 20px;">
      <h3>Products</h3>
    </section>
    @if (session()->has('message'))
      <div class="alert alert-info">
          {{ session('message') }}
      </div>
    @endif
    <div class="row">
      <div class="col-sm-6">
        <form name="uploadExcel" method="POST" enctype="multipart/form-data" action="{{ route('import-file') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label id="excelFile">Import Csv/Excel file</label>
            <input type="file" name="excelFile" id="excelFile" class="form-control" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
            <button type="submit" id="uploadExcel" name="uploadExcel" class="btn btn-primary">Import</button>
          </div>
        </form>
      </div>
      <div class="col-sm-6">
        <a href="{{ url('products/create') }}" class="btn btn-success" style="float: right;">Add new</a>
      </div>

    </div>
    <br>
    <div class="row">
      <div class="col-lg-12">
          <table class="table" id="products-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Sku</th>
                    <th>Price</th>
                    <th>Fee</th>
                    <th>Qty</th>
                    <th>Last Updated</th>
                    <th>Action</th>
                </tr>
            </thead>
          </table>

      </div>


    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('plugins/datatables/JSZip-2.5.0/jszip.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/datatables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/datatables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/datatables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/datatables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/datatables/Buttons-1.5.4/js/dataTables.buttons.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/datatables/Buttons-1.5.4/js/buttons.bootstrap4.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/datatables/Buttons-1.5.4/js/buttons.flash.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/datatables/Buttons-1.5.4/js/buttons.html5.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/product.js') }}"></script>

  <script type="text/javascript">

    $(document).ready(function(){
      var table = $('#products-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('getproducts') }}",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'sku', name: 'sku'},
            {data: 'price', name: 'price'},
            {data: 'install_fee', name: 'install_fee'},
            {data: 'quantity', name: 'quantity'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        dom:"Bfrtip",
        buttons: [{
          extend: 'excel',
            text: 'Save as Excel',
            exportOptions: {
                header: false,
                columns: [ 0, 1, 2, 3, 4 ]
            },
        },
        {
          extend: 'csv',
            text: 'Save as CSV',
            exportOptions: {
                header: false,
                columns: [ 0, 1, 2, 3, 4 ]
            },
        }
        ]
      });
    });
  </script>
@endpush

