# bacancy-test

Php practical test.

## Prerequisites

* [Composer](https://getcomposer.org/)
* [Laravel 5.7](http://laravel.com/)
* [MySQL](https://www.mysql.com/downloads/)

## Installation

Clone project

```
git clone https://gitlab.com/rajesh-parmar/bacancy-test.git
```

Change Directory

```
cd bacancy-test
```

Install all dependencies

```
composer install
```

Create a .env file

```
Create a `.env` file in root of the prject as per `.env.example` supplied.
```

Generate application key

```
php artisan key:generate
```

Create database `bacancytest` and run below command:

```
php artisan migrate
```

Create storage symbolic link:

```
php artisan storage:link
```

Run seeder to add user.

```
php artisan db:seed
```

Serve the application:

```
php artisan serve
```

## Testing

```
email : admin@gmail.com
password : 123456

```

Login with above credentials and visit `products` page.